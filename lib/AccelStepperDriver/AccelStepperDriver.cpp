// AccelStepperDriver.cpp
//
// Copyright (C) 2009-2013 Mike McCauley
// $Id: AccelStepperDriver.cpp,v 1.24 2020/04/20 00:15:03 mikem Exp mikem $

#include "AccelStepperDriver.h"

#if 0
// Some debugging assistance
void dump(uint8_t* p, int l)
{
    int i;

    for (i = 0; i < l; i++)
    {
	Serial.print(p[i], HEX);
	Serial.print(" ");
    }
    Serial.println("");
}
#endif

/***** PUBLIC *****/

AccelStepperDriver::AccelStepperDriver(const uint8_t stepPin, const uint8_t dirPin, const uint8_t enablePin)
{
    _stepPin = stepPin;
    _dirPin = dirPin;
    _enablePin = enablePin;

    _stepInverted = false;
    _dirInverted = false;
    _enableInverted = false;

    _currentPos = 0;
    _targetPos = 0;
    _speed = 0.0;
    _maxSpeed = 1.0;
    _acceleration = 0.0;
    _sqrt_twoa = 1.0;
    _stepInterval = 0;
    _minPulseWidth = 1;
    _lastStepTime = 0;

    // NEW
    _n = 0;
    _c0 = 0.0;
    _cn = 0.0;
    _cmin = 1.0;
    _direction = DIRECTION_CCW;

    // Some reasonable default
    setAcceleration(1);

    pinMode(_stepPin, OUTPUT);
    pinMode(_dirPin, OUTPUT);

    _step1 = &(PORT->Group[g_APinDescription[stepPin].ulPort].OUTSET.reg);
    _step0 = &(PORT->Group[g_APinDescription[stepPin].ulPort].OUTCLR.reg);
    _stepMask = digitalPinToBitMask(stepPin);
    _dir1 = &(PORT->Group[g_APinDescription[dirPin].ulPort].OUTSET.reg);
    _dir0 = &(PORT->Group[g_APinDescription[dirPin].ulPort].OUTCLR.reg);
    _dirMask = digitalPinToBitMask(dirPin);
}

void AccelStepperDriver::moveTo(long absolute)
{
    if (_targetPos != absolute)
    {
        _targetPos = absolute;
        computeNewSpeed();
        // compute new n?
    }
}

void AccelStepperDriver::move(long relative)
{
    moveTo(_currentPos + relative);
}

// Run the motor to implement speed and acceleration in order to proceed to the target position
// You must call this at least once per step, preferably in your main loop
// If the motor is in the desired position, the cost is very small
// returns true if the motor is still running to the target position.
bool AccelStepperDriver::run()
{
    if (runSpeed())
        computeNewSpeed();
    return _speed != 0.0 || distanceToGo() != 0;
}

// Implements steps according to the current step interval
// You must call this at least once per step
// returns true if a step occurred
bool AccelStepperDriver::runSpeed()
{
    // Dont do anything unless we actually have a step interval
    if (!_stepInterval)
        return false;

    unsigned long time = micros();
    if (time - _lastStepTime >= _stepInterval)
    {
        if (_direction == DIRECTION_CW)
        {
            // Clockwise
            _currentPos += 1;
        }
        else
        {
            // Anticlockwise
            _currentPos -= 1;
        }
        step();

        _lastStepTime = time; // Caution: does not account for costs in step()

        return true;
    }
    else
    {
        return false;
    }
}

void AccelStepperDriver::setMaxSpeed(float speed)
{
    if (speed < 0.0)
        speed = -speed;
    if (_maxSpeed != speed)
    {
        _maxSpeed = speed;
        _cmin = 1000000.0 / speed;
        // Recompute _n from current speed and adjust speed if accelerating or cruising
        if (_n > 0)
        {
            _n = (long)((_speed * _speed) / (2.0 * _acceleration)); // Equation 16
            computeNewSpeed();
        }
    }
}

float AccelStepperDriver::maxSpeed()
{
    return _maxSpeed;
}

void AccelStepperDriver::setAcceleration(float acceleration)
{
    if (acceleration == 0.0)
        return;
    if (acceleration < 0.0)
        acceleration = -acceleration;
    if (_acceleration != acceleration)
    {
        // Recompute _n per Equation 17
        _n = _n * (_acceleration / acceleration);
        // New c0 per Equation 7, with correction per Equation 15
        _c0 = 0.676 * sqrt(2.0 / acceleration) * 1000000.0; // Equation 15
        _acceleration = acceleration;
        computeNewSpeed();
    }
}

void AccelStepperDriver::setSpeed(float speed)
{
    if (speed == _speed)
        return;
    speed = constrain(speed, -_maxSpeed, _maxSpeed);
    if (speed == 0.0)
        _stepInterval = 0;
    else
    {
        _stepInterval = fabs(1000000.0 / speed);
        _direction = (speed > 0.0) ? DIRECTION_CW : DIRECTION_CCW;
    }
    _speed = speed;
}

float AccelStepperDriver::speed()
{
    return _speed;
}

long AccelStepperDriver::distanceToGo()
{
    return _targetPos - _currentPos;
}

long AccelStepperDriver::targetPosition()
{
    return _targetPos;
}

long AccelStepperDriver::currentPosition()
{
    return _currentPos;
}

// Useful during initialisations or after initial positioning
// Sets speed to 0
void AccelStepperDriver::setCurrentPosition(long position)
{
    _targetPos = _currentPos = position;
    _n = 0;
    _stepInterval = 0;
    _speed = 0.0;
}

// Blocks until the target position is reached and stopped
void AccelStepperDriver::runToPosition()
{
    while (run())
        YIELD; // Let system housekeeping occur
}

bool AccelStepperDriver::runSpeedToPosition()
{
    if (_targetPos == _currentPos)
        return false;
    if (_targetPos > _currentPos)
        _direction = DIRECTION_CW;
    else
        _direction = DIRECTION_CCW;
    return runSpeed();
}

// Blocks until the new target position is reached
void AccelStepperDriver::runToNewPosition(long position)
{
    moveTo(position);
    runToPosition();
}

void AccelStepperDriver::stop()
{
    if (_speed != 0.0)
    {
        long stepsToStop = (long)((_speed * _speed) / (2.0 * _acceleration)) + 1; // Equation 16 (+integer rounding)
        if (_speed > 0)
            move(stepsToStop);
        else
            move(-stepsToStop);
    }
}

void AccelStepperDriver::setMinPulseWidth(unsigned int minWidth)
{
    _minPulseWidth = minWidth;
}

void AccelStepperDriver::setPinsInverted(bool stepInverted, bool dirInverted, bool enableInverted)
{
    _stepInverted = stepInverted;
    _dirInverted = dirInverted;
    _enableInverted = enableInverted;
}

bool AccelStepperDriver::isRunning()
{
    return !(_speed == 0.0 && _targetPos == _currentPos);
}

void AccelStepperDriver::enable(bool state)
{
    if (_enablePin == 0xFF)
        return;
    pinMode(_enablePin, OUTPUT);
    digitalWrite(_enablePin, state ? LOW : HIGH);
}

/***** PROTECTED *****/

void AccelStepperDriver::computeNewSpeed()
{
    long distanceTo = distanceToGo(); // +ve is clockwise from curent location

    long stepsToStop = (long)((_speed * _speed) / (2.0 * _acceleration)); // Equation 16

    if (distanceTo == 0 && stepsToStop <= 1)
    {
        // We are at the target and its time to stop
        _stepInterval = 0;
        _speed = 0.0;
        _n = 0;
        return;
    }

    if (distanceTo > 0)
    {
        // We are anticlockwise from the target
        // Need to go clockwise from here, maybe decelerate now
        if (_n > 0)
        {
            // Currently accelerating, need to decel now? Or maybe going the wrong way?
            if ((stepsToStop >= distanceTo) || _direction == DIRECTION_CCW)
                _n = -stepsToStop; // Start deceleration
        }
        else if (_n < 0)
        {
            // Currently decelerating, need to accel again?
            if ((stepsToStop < distanceTo) && _direction == DIRECTION_CW)
                _n = -_n; // Start accceleration
        }
    }
    else if (distanceTo < 0)
    {
        // We are clockwise from the target
        // Need to go anticlockwise from here, maybe decelerate
        if (_n > 0)
        {
            // Currently accelerating, need to decel now? Or maybe going the wrong way?
            if ((stepsToStop >= -distanceTo) || _direction == DIRECTION_CW)
                _n = -stepsToStop; // Start deceleration
        }
        else if (_n < 0)
        {
            // Currently decelerating, need to accel again?
            if ((stepsToStop < -distanceTo) && _direction == DIRECTION_CCW)
                _n = -_n; // Start accceleration
        }
    }

    // Need to accelerate or decelerate
    if (_n == 0)
    {
        // First step from stopped
        _cn = _c0;
        _direction = (distanceTo > 0) ? DIRECTION_CW : DIRECTION_CCW;
    }
    else
    {
        // Subsequent step. Works for accel (n is +_ve) and decel (n is -ve).
        _cn = _cn - ((2.0 * _cn) / ((4.0 * _n) + 1)); // Equation 13
        _cn = max(_cn, _cmin);
    }
    _n++;
    _stepInterval = _cn;
    _speed = 1000000.0 / _cn;
    if (_direction == DIRECTION_CCW)
        _speed = -_speed;

#if 0
    Serial.println(_speed);
    Serial.println(_acceleration);
    Serial.println(_cn);
    Serial.println(_c0);
    Serial.println(_n);
    Serial.println(_stepInterval);
    Serial.println(distanceTo);
    Serial.println(stepsToStop);
    Serial.println("-----");
#endif
}

// bit 0 of the mask corresponds to _stepPin
// bit 1 of the mask corresponds to _dirPin
inline void AccelStepperDriver::setOutputPins(bool step, bool dir)
{
    // 	digitalWrite(_pin[i], (mask & (1 << i)) ? (HIGH ^ _pinInverted[i]) : (LOW ^ _pinInverted[i]));
    digitalWrite(_stepPin, step);
    digitalWrite(_dirPin, dir);
}

// Subclasses can override
inline void AccelStepperDriver::step()
{
    /* 
    // 4000 2000 3 revs 2186 ms
    digitalWrite(_dirPin, _direction);
    digitalWrite(_stepPin, HIGH);
    // Caution 200ns setup time
    // Delay the minimum allowed pulse width
    delayMicroseconds(_minPulseWidth);
    digitalWrite(_stepPin, LOW);
    // */

    //*
    // 4000 2000 3 revs 2174 ms
    if(_direction) *_dir1 = _dirMask;
    else *_dir0 = _dirMask;
    *_step1 = _stepMask;
    // Delay the minimum allowed pulse width
    // delayMicroseconds(_minPulseWidth);
    // Without delay 200 ns pulse width
    *_step0 = _stepMask;
    // */
}
