#include "DS2483.h"

/*
Status 0xF0:
DIR TSB SBR RST LL  SD  PPD 1WB
 0   0   0   1   0   1   1   1  0x17

Config 0xC3:
!1WS !SPU !PDN !APU 1WS SPU PDN APU
  1    1    1    0   0   0   0   1  0xE1
*/

DS2483::DS2483()
{
    _timeout = 5000;
}

void DS2483::init()
{
    SerialUSB.println("send 0xF0 (DS2483 reset) to DS2483");
    Wire.beginTransmission(0x18);
    Wire.write(0xF0); // Device Reset
    Wire.endTransmission();
    delay(1);
    SerialUSB.println("send 0xD2 0xE1 (write config) to DS2483");
    Wire.beginTransmission(0x18);
    Wire.write(0xD2); // Write Device Configuration
    Wire.write(0xE1); // see above
    Wire.endTransmission();
    _readPtr = 0xC3; // Device Configuration Register

    Wire.requestFrom(0x18, 1);
    uint8_t config = Wire.read();
    bindump("config", config);
}

void DS2483::setTimeout(uint16_t timeout)
{
    _timeout = timeout;
}

uint8_t DS2483::busyWait()
{
    return busyWait(_timeout);
}

uint8_t DS2483::busyWait(uint16_t timeout) // timeout is µS
{
    if (_readPtr != 0xF0) {
        SerialUSB.println("send 0xE1 0xF0 (set read ptr = status) to DS2483");
        Wire.beginTransmission(0x18);
        Wire.write(0xE1); // Set Read Pointer
        Wire.write(0xF0); // Status Register
        Wire.endTransmission();
        _readPtr = 0xF0; // Status Register
    }
    uint32_t microsec = micros();
    for (;;) {
        Wire.requestFrom(0x18, 1);
        _status = Wire.read();
        // bindump("_status", _status);
        if (timeout == 0) {
            // SerialUSB.println("break - timeout == 0");
            break;
        }
        if (!BIT_READ(_status, 0)) {
            // SerialUSB.println("break - not 1WB");
            break;
        }
        if (micros() - microsec >= timeout) {
            SerialUSB.println("break - timeout");
            break;
        }
    }
    return _status & 0x01;
}

uint8_t DS2483::reset()
{
    uint8_t isBusy = busyWait();
    if (isBusy)
        return isBusy;

    SerialUSB.println("send 0xB4 (1wire reset) to DS2483");
    Wire.beginTransmission(0x18);
    Wire.write(0xB4); // 1-Wire Reset
    Wire.endTransmission();
    _readPtr = 0xF0; // Status Register
    return 0;
}

uint8_t DS2483::select(const uint8_t addrBuf[8])
{
    uint8_t isBusy = busyWait();
    if (isBusy)
        return isBusy;

    SerialUSB.println("send 0x55 (Match ROM) to 1Wire device");
    Wire.beginTransmission(0x18);
    Wire.write(0xA5); // 1-Wire Write Byte
    Wire.write(0x55); // Match ROM
    Wire.endTransmission();

    SerialUSB.println("send addr to 1Wire device:");
    for (int8_t i = 0; i < 8; i++) {
        // Inter-uint8_t 1WB pause
        // Put here so we don't delay in this method after the last uint8_t
        for (;;) {
            Wire.requestFrom(0x18, 1);
            _status = Wire.read();
            // bindump("_status", _status);
            if (!BIT_READ(_status, 0))
                break;
        }

        hexdump("send", addrBuf[i]);
        Wire.beginTransmission(0x18);
        Wire.write(0xA5); // 1-Wire Write Byte
        Wire.write(addrBuf[i]);
        Wire.endTransmission();
    }
    _readPtr = 0xF0; // Status Register
    return 0;
}

uint8_t DS2483::skip()
{
    uint8_t isBusy = busyWait();
    if (isBusy)
        return isBusy;

    SerialUSB.println("send 0xCC (Skip ROM) to 1Wire device");
    Wire.beginTransmission(0x18);
    Wire.write(0xA5); // 1-Wire Write Byte
    Wire.write(0xCC); // Skip ROM
    Wire.endTransmission();

    _readPtr = 0xF0; // Status Register
    return 0;
}

uint8_t DS2483::resume()
{
    uint8_t isBusy = busyWait();
    if (isBusy)
        return isBusy;

    SerialUSB.println("send 0xA5 (Resume) to 1Wire device");
    Wire.beginTransmission(0x18);
    Wire.write(0xA5); // 1-Wire Write Byte
    Wire.write(0xA5); // Resume
    Wire.endTransmission();

    _readPtr = 0xF0; // Status Register
    return 0;
}

uint8_t DS2483::write(uint8_t data)
{
    uint8_t isBusy = busyWait();
    if (isBusy)
        return isBusy;

    hexdump("send uint8_t to 1Wire device", data);
    Wire.beginTransmission(0x18);
    Wire.write(0xA5); // 1-Wire Write Byte
    Wire.write(data);
    Wire.endTransmission();

    _readPtr = 0xF0; // Status Register
    return 0;
}

uint8_t DS2483::read()
{
    uint8_t isBusy = busyWait();
    if (isBusy)
        return 0xFF;

    SerialUSB.println("recv uint8_t from 1Wire device");
    Wire.beginTransmission(0x18);
    Wire.write(0x96); // 1-Wire Read Byte
    Wire.endTransmission();

    for (;;) {
        Wire.requestFrom(0x18, 1);
        _status = Wire.read();
        // bindump("_status", _status);
        if (!BIT_READ(_status, 0))
            break;
    }

    Wire.beginTransmission(0x18);
    Wire.write(0xE1); // Set Read Pointer
    Wire.write(0xE1); // Read Data Register
    Wire.endTransmission();
    Wire.requestFrom(0x18, 1);
    uint8_t data = Wire.read();
    hexdump("recd", data);

    _readPtr = 0xE1; // Read Data Register
    return data;
}

uint8_t DS2483::ll()
{
    if (_readPtr != 0xF0) {
        SerialUSB.println("send 0xE1 0xF0 (set read ptr = status) to DS2483");
        Wire.beginTransmission(0x18);
        Wire.write(0xE1); // Set Read Pointer
        Wire.write(0xF0); // Status Register
        Wire.endTransmission();
        _readPtr = 0xF0; // Status Register
    }

    Wire.requestFrom(0x18, 1);
    _status = Wire.read();
    return BIT_READ(_status, 3);
}

const uint8_t PROGMEM CRC8_table[256] = {
    0x00, 0x5E, 0xBC, 0xE2, 0x61, 0x3F, 0xDD, 0x83, 0xC2, 0x9C, 0x7E, 0x20, 0xA3, 0xFD, 0x1F, 0x41,
    0x9D, 0xC3, 0x21, 0x7F, 0xFC, 0xA2, 0x40, 0x1E, 0x5F, 0x01, 0xE3, 0xBD, 0x3E, 0x60, 0x82, 0xDC,
    0x23, 0x7D, 0x9F, 0xC1, 0x42, 0x1C, 0xFE, 0xA0, 0xE1, 0xBF, 0x5D, 0x03, 0x80, 0xDE, 0x3C, 0x62,
    0xBE, 0xE0, 0x02, 0x5C, 0xDF, 0x81, 0x63, 0x3D, 0x7C, 0x22, 0xC0, 0x9E, 0x1D, 0x43, 0xA1, 0xFF,
    0x46, 0x18, 0xFA, 0xA4, 0x27, 0x79, 0x9B, 0xC5, 0x84, 0xDA, 0x38, 0x66, 0xE5, 0xBB, 0x59, 0x07,
    0xDB, 0x85, 0x67, 0x39, 0xBA, 0xE4, 0x06, 0x58, 0x19, 0x47, 0xA5, 0xFB, 0x78, 0x26, 0xC4, 0x9A,
    0x65, 0x3B, 0xD9, 0x87, 0x04, 0x5A, 0xB8, 0xE6, 0xA7, 0xF9, 0x1B, 0x45, 0xC6, 0x98, 0x7A, 0x24,
    0xF8, 0xA6, 0x44, 0x1A, 0x99, 0xC7, 0x25, 0x7B, 0x3A, 0x64, 0x86, 0xD8, 0x5B, 0x05, 0xE7, 0xB9,
    0x8C, 0xD2, 0x30, 0x6E, 0xED, 0xB3, 0x51, 0x0F, 0x4E, 0x10, 0xF2, 0xAC, 0x2F, 0x71, 0x93, 0xCD,
    0x11, 0x4F, 0xAD, 0xF3, 0x70, 0x2E, 0xCC, 0x92, 0xD3, 0x8D, 0x6F, 0x31, 0xB2, 0xEC, 0x0E, 0x50,
    0xAF, 0xF1, 0x13, 0x4D, 0xCE, 0x90, 0x72, 0x2C, 0x6D, 0x33, 0xD1, 0x8F, 0x0C, 0x52, 0xB0, 0xEE,
    0x32, 0x6C, 0x8E, 0xD0, 0x53, 0x0D, 0xEF, 0xB1, 0xF0, 0xAE, 0x4C, 0x12, 0x91, 0xCF, 0x2D, 0x73,
    0xCA, 0x94, 0x76, 0x28, 0xAB, 0xF5, 0x17, 0x49, 0x08, 0x56, 0xB4, 0xEA, 0x69, 0x37, 0xD5, 0x8B,
    0x57, 0x09, 0xEB, 0xB5, 0x36, 0x68, 0x8A, 0xD4, 0x95, 0xCB, 0x29, 0x77, 0xF4, 0xAA, 0x48, 0x16,
    0xE9, 0xB7, 0x55, 0x0B, 0x88, 0xD6, 0x34, 0x6A, 0x2B, 0x75, 0x97, 0xC9, 0x4A, 0x14, 0xF6, 0xA8,
    0x74, 0x2A, 0xC8, 0x96, 0x15, 0x4B, 0xA9, 0xF7, 0xB6, 0xE8, 0x0A, 0x54, 0xD7, 0x89, 0x6B, 0x35
};

uint8_t DS2483::crc8(uint8_t data, uint8_t crc)
{
    return CRC8_table[(data ^ crc) & 0xFF];
}

uint8_t DS2483::crc8(const uint8_t data[], uint8_t len)
{
    uint8_t crc = 0;
    for (uint8_t i = 0; i < len; i++) {
        crc = CRC8_table[(data[i] ^ crc) & 0xFF];
    }
    return crc;
}

uint8_t DS2483::send(uint8_t romCmd, uint8_t cmd)
{
    return send(romCmd, &cmd, 1, nullptr, 0);
}

uint8_t DS2483::send(uint8_t romCmd, const uint8_t txBuf[], uint8_t txLen)
{
    return send(romCmd, txBuf, txLen, nullptr, 0);
}

uint8_t DS2483::send(uint8_t romCmd, uint8_t cmd, uint8_t rxBuf[], uint8_t rxLen)
{
    return send(romCmd, &cmd, 1, rxBuf, rxLen);
}

uint8_t DS2483::send(uint8_t romCmd, const uint8_t txBuf[], uint8_t txLen, uint8_t rxBuf[], uint8_t rxLen)
{
    reset();
    write(romCmd);

    return _send(txBuf, txLen, rxBuf, rxLen);
}

uint8_t DS2483::send(const uint8_t addrBuf[], uint8_t cmd)
{
    return send(addrBuf, &cmd, 1, nullptr, 0);
}

uint8_t DS2483::send(const uint8_t addrBuf[], const uint8_t txBuf[], uint8_t txLen)
{
    return send(addrBuf, txBuf, txLen, nullptr, 0);
}

uint8_t DS2483::send(const uint8_t addrBuf[], uint8_t cmd, uint8_t rxBuf[], uint8_t rxLen)
{
    return send(addrBuf, &cmd, 1, rxBuf, rxLen);
}

uint8_t DS2483::send(const uint8_t addrBuf[], const uint8_t txBuf[], uint8_t txLen, uint8_t rxBuf[], uint8_t rxLen)
{
    reset();
    if (addrBuf == nullptr)
        skip();
    else
        select(addrBuf);

    return _send(txBuf, txLen, rxBuf, rxLen);
}

uint8_t DS2483::_send(const uint8_t txBuf[], uint8_t txLen, uint8_t rxBuf[], uint8_t rxLen)
{
    // SerialUSB.print("send to 1Wire device ");
    // SerialUSB.print(txLen);
    // SerialUSB.println(" bytes");
    for (int8_t i = 0; i < txLen; i++) {
        write(txBuf[i]);
    }

    SerialUSB.print("recv from 1Wire device ");
    SerialUSB.print(rxLen);
    SerialUSB.println(" bytes");
    for (int8_t i = 0; i < rxLen; i++) {
        rxBuf[i] = read();
    }

    return 0;
}
