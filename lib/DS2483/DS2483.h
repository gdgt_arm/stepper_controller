#pragma once

#include "bit_ops.h"
#include "vardump.h"
#include <Arduino.h>
#include <Wire.h>

#define DS2483_ADDR 0x18

class DS2483 {

public:
    // ----- Constructor -----
    DS2483();

    void init();
    void setTimeout(uint16_t timeout); // timeout is µS
    uint8_t busyWait(); // timeout is _timeout
    uint8_t busyWait(uint16_t timeout); // timeout is µS
    uint8_t reset();
    uint8_t select(const uint8_t addrBuf[8]);
    uint8_t skip();
    uint8_t resume();
    uint8_t write(uint8_t data);
    uint8_t read();
    uint8_t ll();
    uint8_t crc8(uint8_t data, uint8_t crc = 0);
    uint8_t crc8(const uint8_t data[], uint8_t len);
    uint8_t send(uint8_t romCmd, uint8_t cmd);
    uint8_t send(uint8_t romCmd, const uint8_t txBuf[], uint8_t txLen);
    uint8_t send(uint8_t romCmd, uint8_t cmd, uint8_t rxBuf[], uint8_t rxLen);
    uint8_t send(uint8_t romCmd, const uint8_t txBuf[], uint8_t txLen, uint8_t rxBuf[], uint8_t rxLen);
    uint8_t send(const uint8_t addrBuf[], uint8_t cmd);
    uint8_t send(const uint8_t addrBuf[], const uint8_t txBuf[], uint8_t txLen);
    uint8_t send(const uint8_t addrBuf[], uint8_t cmd, uint8_t rxBuf[], uint8_t rxLen);
    uint8_t send(const uint8_t addrBuf[], const uint8_t txBuf[], uint8_t txLen, uint8_t rxBuf[], uint8_t rxLen);

private:
    uint16_t _timeout; // Default = 1000 µS
    uint8_t _readPtr;
    uint8_t _status;

    uint8_t _send(const uint8_t txBuf[], uint8_t txLen, uint8_t rxBuf[], uint8_t rxLen);
};
