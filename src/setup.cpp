#include "setup.h"

uint8_t startCAN()
{ // Start CAN
    if (CAN.begin(MCP_ANY, CAN_BUS_SPEED, MCP_8MHZ) != CAN_OK) {
        SerialUSB.print("Failed to start CAN");
        return 1;
    }
    CAN.setMode(MCP_NORMAL); // Set operation mode to normal so the MCP2515 sends acks to received data.
    pinMode(CANINT, INPUT); // Setting pin for /INT input
    SerialUSB.println(F("CAN started"));

    CAN.init_Mask(0, 0, 0x700); // init mask - Only care about the first three bits
    CAN.init_Filt(0, 0, CAN_ALERT); // init filter - Want alerts (000 xxxx xxxx)
    CAN.init_Filt(1, 0, CAN_TS); // init filter - Want arm timeslots (001 xxxx xxxx)
    CAN.init_Mask(1, 0, 0x700); // init mask - Only care about the first three bits
    CAN.init_Filt(2, 0, CAN_ARM); // init filter - Want commands (100 xxxx xxxx)
    CAN.init_Filt(3, 0, CAN_STATUS); // init filter - Want statuses (101 xxxx xxxx)

    serprintln(F("CAN mask and filter set"));

    return 0;
}
