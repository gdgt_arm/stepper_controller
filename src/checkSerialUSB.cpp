#include "checkSerialUSB.h"

extern DelayTimer dtBlink;
extern Adafruit_NeoPixel strip;
extern uint8_t neoColor[3];
extern DS2483 ds1wire;
extern struct STATUS_DATA driverStatus;
// extern bool driverStatusIsFresh;
extern const uint8_t addrWrist[] = { 0xDD, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x93 };
extern const uint8_t addrGripper[] = { 0xEE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0 };
extern const uint8_t addrPivot[] = { 0x77, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60 };
extern const char jointNames[NUM_OF_DOF] = { 'S', 'E', 'W', 'R', 'P' };

uint8_t serialInputState = SERIAL_STATE_IDLE;
uint8_t dofIndex = 0;
char rxBufUSB[10];
uint8_t rxBufIndex;

int16_t moveToParams[6] = { 40, 130, 480, 0, 90 };
uint8_t velocityFactor = 1;
struct MOVETO_DATA moveToUSB = { 0, 0, 0, 0, 0, 1 };

void checkSerialUSB()
{
    uint8_t buf[10];

    if (SerialUSB.available() > 0) {
        LED_ON();
        dtBlink.reset(0, 10);
        char rc = SerialUSB.read();
        if (rc >= 'a' && rc <= 'z')
            rc -= 32; // Convert a-z to A-Z
        SerialUSB.print(rc);
        uint8_t ri = (rc >= '0' && rc <= '9') ? rc - 48 : 0xff;
        switch (serialInputState) {
        case SERIAL_STATE_IDLE:
            switch (rc) {
            case 'L':
                SerialUSB.println(" Driver Limp");
                txSerial(ARM_CMD_LIMP);
                break;
            case 'I':
                SerialUSB.println(" Driver Init");
                txSerial(ARM_CMD_INIT);
                break;
            case 'P':
                SerialUSB.println(" Driver Park");
                txSerial(ARM_CMD_PARK);
                break;
            case 'U':
                SerialUSB.println(" Driver Unpark");
                txSerial(ARM_CMD_UNPARK);
                break;
            case 'E':
                SerialUSB.println(" Driver Extend");
                txSerial(ARM_CMD_EXTEND);
                break;
            case 'M':
                SerialUSB.print("oveTo ");
                serialInputState = SERIAL_STATE_MOVETO_PARAM;
                break;
            case 'V':
                SerialUSB.print("elocity factor ");
                serialInputState = SERIAL_STATE_VEL_FACTOR;
                break;
            case 'G':
                SerialUSB.println("o world coord");
                moveToUSB.x = moveToParams[0] - WORLD_OFFSET_X;
                moveToUSB.y = moveToParams[1] - WORLD_OFFSET_Y;
                moveToUSB.z = moveToParams[2] - WORLD_OFFSET_Z;
                moveToUSB.angR = moveToParams[3];
                moveToUSB.angG = moveToParams[4];
                moveToUSB.velocityFactor = velocityFactor;
                txSerial(ARM_CMD_MOVETO, (uint8_t*)&moveToUSB, sizeof(moveToUSB));
                break;
            case 'H':
                SerialUSB.println(" go arm coord");
                moveToUSB.x = moveToParams[0];
                moveToUSB.y = moveToParams[1];
                moveToUSB.z = moveToParams[2];
                moveToUSB.angR = moveToParams[3];
                moveToUSB.angG = moveToParams[4];
                moveToUSB.velocityFactor = velocityFactor;
                txSerial(ARM_CMD_MOVETO, (uint8_t*)&moveToUSB, sizeof(moveToUSB));
                break;
            case '\\': {
                // moveToParams[5] = velocityFactor;
                // handleCmd(ARM_CMD_GRAB_CAN, moveToParams);
                grabCan();
                break;
            }
            case 'W':
                SerialUSB.println("here");
                where();
                break;
            case ' ':
                SerialUSB.println("STOP");
                handleCmd(ARM_CMD_STOP);
                break;
            case 'S':
                SerialUSB.println("tatus");
                txSerial(ARM_CMD_STATUS);
                break;
            case '[':
                SerialUSB.println(" home wrist");
                handleCmd(ARM_CMD_HOME_R);
                break;
            case ']':
                SerialUSB.println(" home pivot");
                handleCmd(ARM_CMD_HOME_P);
                break;
            case 'D':
                SerialUSB.println("S2483 init");
                ds1wire.init();
                break;
            case 'B':
                SerialUSB.println("usyWait");
                ds1wire.busyWait();
                break;
            case 'R':
                SerialUSB.println("eset");
                ds1wire.reset();
                ds1wire.busyWait();
                break;
            case 'N':
                SerialUSB.println("EO");
                buf[0] = 0xCA;
                buf[1] = 0x32;
                buf[2] = 0x00;
                buf[3] = 0x00;
                buf[4] = ds1wire.crc8(&buf[1], 3);
                ds1wire.send(addrWrist, buf, 5);
                break;
            case 'F':
                SerialUSB.println(" Hall follow");
                buf[0] = 0x56;
                ds1wire.send(addrWrist, buf, 1);
                break;
            case 'K':
                SerialUSB.println(" Solenoid");
                ds1wire.send(addrWrist, 0x96);
                delay(2000);
                ds1wire.send(addrWrist, 0x93);
                break;
            case 'O':
                SerialUSB.println("pen");
                buf[0] = 0x3A;
                buf[1] = 255;
                buf[2] = ds1wire.crc8(buf[1]);
                ds1wire.send(addrGripper, buf, 3);
                break;
            case 'C':
                SerialUSB.println("lose");
                buf[0] = 0x3A;
                buf[1] = 1;
                buf[2] = ds1wire.crc8(buf[1]);
                ds1wire.send(addrGripper, buf, 3);
                break;
            case '0':
                SerialUSB.println(" LED OFF");
                ds1wire.send(nullptr, 0xC3);
                break;
            case '1':
                SerialUSB.println(" LED ON");
                ds1wire.send(nullptr, 0xC6);
                break;
            // case 'N':
            //     SerialUSB.print("EO ");
            //     serialInputState = SERIAL_STATE_NEO_COLOR;
            //     break;
            // case '0':
            //     SerialUSB.println(" LED OFF");
            //     txSerial(ARM_CMD_LED, 0);
            //     break;
            // case '1':
            //     SerialUSB.println(" LED ON");
            //     txSerial(ARM_CMD_LED, 1);
            //     break;
            case '2':
                SerialUSB.println(" NEO OFF");
                buf[0] = 0;
                buf[1] = 0;
                buf[2] = 0;
                txSerial(ARM_CMD_NEO, buf, 3);
                break;
            case '3':
                SerialUSB.println(" NEO ON");
                buf[0] = 0;
                buf[1] = 0;
                buf[2] = 16;
                txSerial(ARM_CMD_NEO, buf, 3);
                break;
            case '4':
                SerialUSB.println(" Ref LEDs Off");
                buf[0] = 0x53;
                ds1wire.send(addrGripper, buf, 1);
                break;
            case '5':
                SerialUSB.println(" Ref LEDs On");
                buf[0] = 0x56;
                ds1wire.send(addrGripper, buf, 1);
                break;

            case '?':
            case '/':
                SerialUSB.println();
                SerialUSB.println(F("i = init"));
                SerialUSB.println(F("l = limp"));
                SerialUSB.println(F("p = park"));
                SerialUSB.println(F("u = unpark"));
                SerialUSB.println(F("e = extend"));
                SerialUSB.println(F("m = move to"));
                SerialUSB.println(F("    x, y, z = mm"));
                SerialUSB.println(F("    r = rotate angle"));
                SerialUSB.println(F("    g = gripper angle"));
                SerialUSB.println(F("v = velocity"));
                SerialUSB.println(F("g = go world coord"));
                SerialUSB.println(F("h = go arm coord"));
                SerialUSB.println(F("w = where"));
                SerialUSB.println(F("<space> = stop"));
                SerialUSB.println(F("s = status"));
                SerialUSB.println(F("[ = home wrist"));
                SerialUSB.println(F("] = home pivot"));
                SerialUSB.println(F("\\ = can"));
                SerialUSB.println();
                SerialUSB.println(F("d = DS2483 init"));
                SerialUSB.println(F("b = busywait"));
                SerialUSB.println(F("r = reset"));
                SerialUSB.println(F("n = NEO wrist red"));
                SerialUSB.println(F("f = follow halls"));
                SerialUSB.println(F("k = solenoid 2 secs"));
                SerialUSB.println(F("o = open"));
                SerialUSB.println(F("c = close"));
                SerialUSB.println();
                SerialUSB.println(F("0 = LEDs off"));
                SerialUSB.println(F("1 = LEDs on"));
                SerialUSB.println(F("2 = NEO driver off"));
                SerialUSB.println(F("3 = NEO driver on"));
                SerialUSB.println(F("4 = Ref LEDs off"));
                SerialUSB.println(F("5 = Ref LEDs on"));
                break;
            case '\n':
            case '\r':
                SerialUSB.println("Ready");
                break;
            default:
                SerialUSB.println(F(" INVALID"));
            }
            rxBufIndex = 0;
            break;
        case SERIAL_STATE_MOVETO_PARAM:
            switch (rc) {
            case 'X':
                dofIndex = 0;
                SerialUSB.print(" mm ");
                serialInputState = SERIAL_STATE_MOVETO_VALUE;
                break;
            case 'Y':
                dofIndex = 1;
                SerialUSB.print(" mm ");
                serialInputState = SERIAL_STATE_MOVETO_VALUE;
                break;
            case 'Z':
                dofIndex = 2;
                SerialUSB.print(" mm ");
                serialInputState = SERIAL_STATE_MOVETO_VALUE;
                break;
            case 'R':
                dofIndex = 3;
                SerialUSB.print("otate angle ");
                serialInputState = SERIAL_STATE_MOVETO_VALUE;
                break;
            case 'G':
                dofIndex = 4;
                SerialUSB.print("ripper angle ");
                serialInputState = SERIAL_STATE_MOVETO_VALUE;
                break;
            default:
                SerialUSB.println(F(" Invalid parameter"));
                serialInputState = SERIAL_STATE_IDLE;
            }
            break;
        case SERIAL_STATE_NEO_COLOR:
            switch (rc) {
            case 'R':
                dofIndex = 0;
                serialInputState = SERIAL_STATE_NEO_VALUE;
                SerialUSB.print(F("ed "));
                break;
            case 'G':
                dofIndex = 1;
                serialInputState = SERIAL_STATE_NEO_VALUE;
                SerialUSB.print(F("reen "));
                break;
            case 'B':
                dofIndex = 2;
                serialInputState = SERIAL_STATE_NEO_VALUE;
                SerialUSB.print(F("lue "));
                break;
            case 'W':
                dofIndex = 3;
                serialInputState = SERIAL_STATE_NEO_VALUE;
                SerialUSB.print(F("hite "));
                break;
            default:
                serialInputState = SERIAL_STATE_IDLE;
                SerialUSB.println(F(" Invalid color"));
            }
            rxBufIndex = 0;
            break;
        case SERIAL_STATE_MOVETO_VALUE:
        case SERIAL_STATE_NEO_VALUE:
        case SERIAL_STATE_VEL_FACTOR:
            if ((ri < 10 || rc == '.' || rc == '-') && rxBufIndex < 9) {
                rxBufUSB[rxBufIndex] = rc;
                rxBufIndex++;
            } else if (rc == 0x08) { // backspace
                rxBufIndex--;
                SerialUSB.print(' ');
                SerialUSB.write(0x08);
            } else if (rc == '\n' || rc == '\r') {
                rxBufUSB[rxBufIndex] = 0; // NULL-terminated C-string
                SerialUSB.println();
                if (serialInputState == SERIAL_STATE_MOVETO_VALUE) {
                    int16_t v = atoi(rxBufUSB);
                    moveToParams[dofIndex] = v;
                } else if (serialInputState == SERIAL_STATE_NEO_VALUE) {
                    int16_t v = atoi(rxBufUSB);
                    if (v >= 0 && v < 256) {
                        if (dofIndex == 3) {
                            neoColor[0] = v;
                            neoColor[1] = v;
                            neoColor[2] = v;
                        } else {
                            neoColor[dofIndex] = v;
                        }
                        strip.fill(strip.Color(neoColor[0], neoColor[1], neoColor[2]));
                        strip.show();
                        strip.show();
                    } else
                        SerialUSB.println(F("Invalid value"));
                } else if (serialInputState == SERIAL_STATE_VEL_FACTOR) {
                    int16_t v = atoi(rxBufUSB);
                    if (v > 0 && v <= 16) {
                        velocityFactor = v;
                    } else
                        SerialUSB.println(F("Invalid value"));
                }
                serialInputState = SERIAL_STATE_IDLE;
            } else {
                SerialUSB.println(F(" Invalid key - try again"));
                serialInputState = SERIAL_STATE_IDLE;
            }
            break;
        }
    }
}

void where()
{
    SerialUSB.println();

    SerialUSB.print(F("X: "));
    SerialUSB.println(moveToParams[0]);
    SerialUSB.print(F("Y: "));
    SerialUSB.println(moveToParams[1]);
    SerialUSB.print(F("Z: "));
    SerialUSB.println(moveToParams[2]);
    SerialUSB.print(F("R: "));
    SerialUSB.println(moveToParams[3]);
    SerialUSB.print(F("G: "));
    SerialUSB.println(moveToParams[4]);
    SerialUSB.println();
}
