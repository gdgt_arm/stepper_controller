#include "checkSerial.h"

extern DelayTimer dtBlink;
extern int16_t targetDegrees[NUM_OF_DOF];
extern Adafruit_NeoPixel strip;
extern uint8_t velocityFactor;
extern uint8_t armState;
extern bool isMoving;
extern struct STATUS_DATA driverStatus;
bool driverStatusIsFresh;

uint8_t rxBuf[sizeof(STATUS_DATA)];
uint8_t rxPtr = 0;
uint8_t rxState = RX_IDLE;
uint8_t rxBytes;
bool rxEscRecd;

// union {
//     uint8_t buf[sizeof(STATUS_DATA)]; // STATUS_DATA is the biggest thing we are puttting in txData
//     struct STATUS_DATA status;
// } rxData;

/********************************************************
   Incoming Serial Command Functions
********************************************************/

void checkSerial()
{
    if (Serial.available() > 0) {
        LED_ON();
        dtBlink.reset(0, 10);
        uint8_t rc = Serial.read();
        // hexdump("recd", rc);

        if (rc == PACKET_START) {
            rxPtr = 0;
            rxState = RX_START_RECD;
            return;
        }

        if (rxState == RX_IDLE)
            return; // Still waiting for a start byte

        if (rc == PACKET_ESC) {
            rxEscRecd = true;
            return;
        }

        if (rxEscRecd) {
            rc = rc ^ PACKET_XOR;
            rxEscRecd = false;
        }

        rxBuf[rxPtr] = rc;
        rxPtr++;
        if (rxPtr == sizeof(STATUS_DATA)) {
            memcpy(&driverStatus, rxBuf, sizeof(STATUS_DATA));
            driverStatusIsFresh = true;
            rxState = RX_IDLE;

            // serprintln("\nStatus recd:");
            // vardump("S", driverStatus.fAng[0]);
            // vardump("E", driverStatus.fAng[1]);
            // vardump("W", driverStatus.fAng[2]);
            // vardump("R", driverStatus.fAng[3]);
            // vardump("P", driverStatus.fAng[4]);
            // vardump("state", driverStatus.state);
            // vardump("moving", driverStatus.moving);
            // vardump("err", driverStatus.err);
            // serprintln();
        }
    }
}

void txSerial(uint8_t cmd, int16_t msDelay)
{
    txSerial(cmd, nullptr, 0, msDelay);
}

void txSerial(uint8_t cmd, uint8_t val, int16_t msDelay)
{
    txSerial(cmd, &val, 1, msDelay);
}

void txSerial(uint8_t cmd, uint8_t buf[], uint8_t bytes, int16_t msDelay)
{
    Serial.write(PACKET_START);
    // hexdump("Send cmd", cmd);
    Serial.write(cmd);
    for (uint8_t i = 0; i < bytes; i++) {
        // hexdump("Send data", buf[i]);
        if (buf[i] == PACKET_START || buf[i] == PACKET_ESC) {
            Serial.write(PACKET_ESC);
            Serial.write(buf[i] ^ PACKET_XOR);
        } else {
            Serial.write(buf[i]);
        }
    }
    driverStatusIsFresh = false;
    while (!driverStatusIsFresh)
        checkSerial();

    if (msDelay > -1) {
        waitWhileMoving();
        if (msDelay > 0)
            delay(msDelay);
    }
}

void getStatus()
{
    // serprintln("get status");
    txSerial(ARM_CMD_STATUS);
    // while (!driverStatusIsFresh)
    //     checkSerial();
}

void waitWhileMoving()
{
    while (driverStatus.moving) {
        while (!driverStatusIsFresh)
            checkSerial();
        // delay(100);
        getStatus();
    }
}