#ifndef CHECKSERIALUSB_H
#define CHECKSERIALUSB_H

#include <Arduino.h>
#include "main.h"

#define SERIAL_STATE_IDLE 0
#define SERIAL_STATE_K_M_ID 1
#define SERIAL_STATE_K_P_ID 2
#define SERIAL_STATE_K_VALUE 3
#define SERIAL_STATE_JOINT_NUM 4
#define SERIAL_STATE_JOINT_ANGLE 5
#define SERIAL_STATE_MOVETO_PARAM 6
#define SERIAL_STATE_MOVETO_VALUE 7
#define SERIAL_STATE_NEO_COLOR 8
#define SERIAL_STATE_NEO_VALUE 9
#define SERIAL_STATE_VEL_FACTOR 10

void checkSerialUSB();
void where();

#endif