#ifndef MAIN_H
#define MAIN_H

#include <Adafruit_NeoPixel.h>
#include <Arduino.h>
#include <Wire.h>

#include "DelayTimer.h"
#include "mcp_can.h"
#include "serialusb.print.h"
// #include "settings.h"
#include "1wire_node_commands.h"
#include "DS2483.h"
#include "fastRunningMedian5.h"
#include "stepper_controller_driver.h"
#include "vardump.h"
#include "checkSerial.h"
#include "checkSerialUSB.h"

#include "speak.h"
#include "CAN_IDs.h"
#include "tts_groups.h"

#define LED1 1
#define LED0 0
#define LED_ON() digitalWrite(LED_BUILTIN, LED1)
#define LED_OFF() digitalWrite(LED_BUILTIN, LED0)
#define SW_OPEN 1
#define SW_CLOSED 0

#define STATUS_OK 0

#define CAN_SPI_INT_PIN 2

#define NO_PRINT

#define MOTION_IDLE 0
#define MOTION_OPEN_GRIPPER 1
#define MOTION_WAIT_CAN_FOCUS 2
#define MOTION_MOVEING_NEAR_CAN 3

#define GRAB_CAN_IDLE 0
#define GRAB_CAN_RUNNING 1
#define GRAB_CAN_SUCCESS 2
#define GRAB_CAN_DRV_ERR 3

const int16_t WORLD_OFFSET_X = 0;
const int16_t WORLD_OFFSET_Y = 271;
const int16_t WORLD_OFFSET_Z = 58;

/********************************************************
   Function Prototypes
********************************************************/

void yield();
void yDelay(uint32_t msDelay);

void checkCAN();
void sendCANMsg(unsigned long id);
void sendCANMsg(unsigned long id, uint8_t len, uint8_t *buf);
uint8_t handleCAN(unsigned long id, uint8_t rr, uint8_t cmdBuf[], uint8_t len);

void handleCmd(uint8_t cmd, int16_t params[] = {});
void grabCan();

uint8_t getHallState();

#endif
