#include "main.h"

#include "setup.h"

/********************************************************
   Global Variables
********************************************************/

uint8_t errnum = STATUS_OK;

Adafruit_NeoPixel strip(1, PIN_NEO, NEO_GRB + NEO_KHZ800);
uint8_t neoColor[3] = { 0, 0, 0 };

DelayTimer dtBlink;

DS2483 ds1wire;

MCP_CAN CAN(PIN_SPI_CS);
uint8_t CANbuf[CAN_MAX_CHAR_IN_MESSAGE];

struct STATUS_DATA driverStatus;

struct MOVETO_DATA moveTo = { 0, 0, 0, 0, 0, 1 };

uint32_t lastCanUpdate = 0;
uint32_t lastGripUpdate = 0;
FastRunningMedian5<int16_t> medianCanX;
FastRunningMedian5<int16_t> medianCanY;
FastRunningMedian5<int16_t> medianCanZ;
FastRunningMedian5<int16_t> medianGripX;
FastRunningMedian5<int16_t> medianGripY;
FastRunningMedian5<int16_t> medianGripZ;
uint8_t grabCanStatus = GRAB_CAN_IDLE;

uint8_t buf[100];

const uint8_t addrWrist[] = { 0xDD, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x93 };
const uint8_t addrGripper[] = { 0xEE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xC0 };
const uint8_t addrPivot[] = { 0x77, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x60 };
const char jointNames[NUM_OF_DOF] = { 'S', 'E', 'W', 'R', 'P' };

/********************************************************
   Setup
********************************************************/

void setup()
{
    // Give us something pretty to look at.
    pinMode(LED_BUILTIN, OUTPUT); // Set Heartbeat LED as output
    LED_ON(); // Turn LED on

    strip.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
    strip.show();

    // Open a serial connection and wait for a keypress.
    SerialUSB.begin(SERBAUD_USB);
    int msWait = millis();
    SerialUSB.println(F("\nWaiting for MOTU..."));
    while (!SerialUSB.available() && millis() - msWait < 2000)
        ;
    SerialUSB.println(F("\nStarting..."));

    errnum = startCAN(); // Start the CAN interface
    if (errnum)
        return;

    Serial.begin(SERBAUD_CONTROLLER_DRIVER);

    Wire.begin();

    ds1wire.init();

    // LED_OFF();
    dtBlink.setDelay(1);
    strip.show(); // Turn OFF all pixels ASAP
}

/********************************************************
   Loop
********************************************************/

void loop()
{
    uint32_t msNow = millis();

    if (errnum) {
        if (dtBlink.tripped(msNow)) {
            digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
            dtBlink.reset(msNow, 200);
        }
        return;
    }

    yield();
}

void yield()
{
    uint32_t msNow = millis();

    checkCAN();
    checkSerial();
    checkSerialUSB();

    if (dtBlink.tripped(msNow)) {
        if (digitalRead(LED_BUILTIN) == LED1) {
            LED_OFF();
            dtBlink.reset(msNow, 1990);
        } else {
            LED_ON();
            dtBlink.reset(msNow, 10);
        }
    }
}

void yDelay(uint32_t msDelay)
{
    uint32_t msStart = millis();

    while (millis() - msStart < msDelay)
        yield();
}

/********************************************************
   1Wire
********************************************************/

/********************************************************
   CAN Functions
********************************************************/

void checkCAN()
{
    if (!digitalRead(CAN_SPI_INT_PIN)) {
        uint16_t id;
        uint8_t len;
        uint8_t rr;

        // iterate over all pending messages
        // If either the bus is saturated or the MCU is busy,
        // both RX buffers may be in use and reading a single
        // message does not clear the IRQ conditon.
        while (CAN.checkReceive() == CAN_MSGAVAIL) {
            unsigned long rx_id;
            CAN.readMsgBuf(&rx_id, &len, CANbuf);
            rr = rx_id & 0x40000000 ? 1 : 0;
            id = rx_id;
#ifndef NO_PRINT
            char buf[100];
            uint8_t p = sprintf(buf, "Rx: %03X rr: %u len: %u", id, rr, len);
            uint8_t i = 0;
            while (len) {
                char b[3];
                sprintf(b, "%02X", CANbuf[i]);
                buf[p] = ' ';
                p++;
                buf[p] = b[0];
                p++;
                buf[p] = b[1];
                p++;
                i++;
                len--;
            }
            buf[p] = 0;

            serprintln(buf);
#endif
            handleCAN(id, rr, CANbuf, len);
        }
    }
}

void sendCANMsg(unsigned long id)
{
    LED_ON();
    dtBlink.setDelay(500);
    CAN.sendMsgBuf(id, 0, 0, nullptr);
}

void sendCANMsg(unsigned long id, uint8_t len, uint8_t* buf)
{
    LED_ON();
    dtBlink.setDelay(500);
    CAN.sendMsgBuf(id, 0, len, buf);
}

uint8_t handleCAN(unsigned long id, uint8_t rr, uint8_t data[], uint8_t len)
{
    if (id == CAN_STATUS_ARM && rr) {
        getStatus();
        CAN.sendMsgBuf(CAN_STATUS_ARM, 0, 3, &driverStatus.state);
    } else if ((id & 0x700) == CAN_ARM) {
        uint8_t cmd = id & 0x0FF;
        vd("cmd", cmd);

        switch (cmd) {
        case ARM_CMD_MOVETO:
            // Store the passed in params and send to driver if told to do so.
            memcpy(&moveTo.x, data + 1, 2);
            moveTo.x -= WORLD_OFFSET_X;
            memcpy(&moveTo.y, data + 3, 2);
            moveTo.y -= WORLD_OFFSET_Y;
            memcpy(&moveTo.z, data + 5, 2);
            moveTo.z -= WORLD_OFFSET_Z;
            if (data[7])
                txSerial(ARM_CMD_MOVETO, (uint8_t*)&moveTo, sizeof(moveTo));
            break;
        case (ARM_CMD_MOVETO + 1):
            // Store the passed in params and send to driver if told to do so.
            memcpy(&moveTo.angR, data + 1, 2);
            memcpy(&moveTo.angG, data + 3, 1);
            memcpy(&moveTo.velocityFactor, data + 4, 1);
            if (data[5])
                txSerial(ARM_CMD_MOVETO, (uint8_t*)&moveTo, sizeof(moveTo));
            break;
        case ARM_CMD_HOME:
        case ARM_CMD_HOME_R:
        case ARM_CMD_HOME_P:
        case ARM_CMD_EXTEND:
        case ARM_CMD_DOOR_EXT:
        case ARM_CMD_DOOR_PRESS:
        case GRIP_CMD_OPEN:
        case GRIP_CMD_CLOSE:
        case ARM_CMD_TOOL_READY:
            handleCmd(cmd);
            break;
        case GRIP_CMD_REF_LED:
            buf[0] = data[0] ? 0x56 : 0x53;
            ds1wire.send(addrGripper, buf, 1);
            break;
        case ARM_CMD_GRAB_CAN: {
            grabCan();
            vardump("grabCanStatus", grabCanStatus);
            sendCANMsg(CAN_STATUS_ARM, 1, &grabCanStatus);
            break;
        }
        case ARM_CMD_SEQUENCE:
            for (uint8_t i = 0; i < len; i++) {
                handleCmd(data[i]);
                delay(1000);
            }
            break;
        default:
            if (cmd < ARM_CMD_DISABLE) {
                txSerial(cmd);
            }
        }
    } else if (id == CAN_TS_CAN) {
        // Can
        int16_t X = (data[0] << 8 | data[1]);
        int16_t Y = (data[2] << 8 | data[3]);
        int16_t Z = (data[4] << 8 | data[5]);

        if (Z == 0) {
            // serprintln("can median reset");
            medianCanX.reset();
            medianCanY.reset();
            medianCanZ.reset();
        } else if (medianCanX.isFull() && (abs(medianCanX.getMedian() - X) > 20 || abs(medianCanY.getMedian() - Y) > 20 || abs(medianCanZ.getMedian() - Z) > 20)) {
            // serprintln("can moved - median reset");
            medianCanX.reset();
            medianCanY.reset();
            medianCanZ.reset();
        } else {
            // serprintln("can median add");
            medianCanX.addValue(X);
            medianCanY.addValue(Y);
            medianCanZ.addValue(Z);
            lastCanUpdate = millis();
        }
    } else if (id == CAN_TS_GRIP) {
        // Gripper
        int16_t X = (data[0] << 8 | data[1]);
        int16_t Y = (data[2] << 8 | data[3]);
        int16_t Z = (data[4] << 8 | data[5]);

        if (Z == 0) {
            // serprintln("grip median reset");
            medianGripX.reset();
            medianGripY.reset();
            medianGripZ.reset();
        } else if (medianGripX.isFull() && (abs(medianGripX.getMedian() - X) > 20 || abs(medianGripY.getMedian() - Y) > 20 || abs(medianGripZ.getMedian() - Z) > 20)) {
            // serprintln("grip moved - median reset");
            medianGripX.reset();
            medianGripY.reset();
            medianGripZ.reset();
        } else {
            // serprintln("grip median add");
            medianGripX.addValue(X);
            medianGripY.addValue(Y);
            medianGripZ.addValue(Z);
            lastGripUpdate = millis();
        }
    }
    return 1;
}

/********************************************************
   Arm Functions
********************************************************/

void handleCmd(uint8_t cmd, int16_t params[])
{
    vd("handleCmd", cmd);
    switch (cmd) {
    case ARM_CMD_HOME:
        handleCmd(ARM_CMD_HOME_R);
        handleCmd(ARM_CMD_HOME_P);
        break;
    case ARM_CMD_HOME_R: {
        // get status
        getStatus();
        // if not init'd
        if (!driverStatus.state) {
            serprintln("ARM_CMD_INIT");
            txSerial(ARM_CMD_INIT);
        }
        waitWhileMoving();

        ds1wire.send(addrWrist, OW_WRIST_REPORT_HALL);
        ds1wire.busyWait();

        // Move until the Hall sensor trips
        struct JOG_DATA jogData;
        jogData.joint = JOINT_ROTATE;

        serprintln("ARM_CMD_JOG Move until the Hall sensor trips");
        jogData.fDelta = driverStatus.fAng[JOINT_ROTATE] > 0 ? -360.0 : 360;
        jogData.velocityFactor = 2;
        txSerial(ARM_CMD_JOG, (uint8_t*)&jogData, sizeof(jogData));

        while (ds1wire.ll()) { }

        serprintln("ARM_CMD_STOP");
        txSerial(ARM_CMD_STOP);

        // Now more slowly the other direction
        serprintln("ARM_CMD_JOG Move more slowly");
        jogData.fDelta = 10;
        jogData.velocityFactor = 4;
        txSerial(ARM_CMD_JOG, (uint8_t*)&jogData, sizeof(jogData));

        while (!ds1wire.ll()) { }

        serprintln("ARM_CMD_STOP");
        txSerial(ARM_CMD_STOP);

        serprintln("ARM_CMD_JOG Move to center/zero position");
        jogData.fDelta = 7.0;
        jogData.velocityFactor = 4;
        txSerial(ARM_CMD_JOG, (uint8_t*)&jogData, sizeof(jogData), 0);

        serprintln("ARM_CMD_SETPOS");
        struct SETPOS_DATA setPosData;
        setPosData.joint = JOINT_ROTATE;
        setPosData.fAng = 0.0;
        txSerial(ARM_CMD_SETPOS, (uint8_t*)&setPosData, sizeof(setPosData));
        break;
    }
    case ARM_CMD_HOME_P: {
        // get status
        getStatus();
        // if not init'd
        if (!driverStatus.state) {
            serprintln("ARM_CMD_INIT");
            txSerial(ARM_CMD_INIT);
        }
        waitWhileMoving();

        ds1wire.send(addrPivot, OW_PIVOT_REPORT_SENSOR);
        ds1wire.busyWait();

        // Move until the relective sensor trips
        struct JOG_DATA jogData;
        jogData.joint = JOINT_PIVOT;

        if (ds1wire.ll()) {
            serprintln("ARM_CMD_JOG Move until the relective sensor trips");
            jogData.fDelta = -30.0;
            jogData.velocityFactor = 4;
            txSerial(ARM_CMD_JOG, (uint8_t*)&jogData, sizeof(jogData));

            while (ds1wire.ll()) { }
        }

        serprintln("ARM_CMD_STOP");
        txSerial(ARM_CMD_STOP);

        // Now more slowly the other direction
        serprintln("ARM_CMD_JOG Move until the relective sensor releases");
        jogData.fDelta = 10;
        jogData.velocityFactor = 8;
        txSerial(ARM_CMD_JOG, (uint8_t*)&jogData, sizeof(jogData));

        while (!ds1wire.ll()) { }

        serprintln("ARM_CMD_STOP");
        txSerial(ARM_CMD_STOP);

        serprintln("ARM_CMD_JOG Move to center/zero position");
        jogData.fDelta = 1.5;
        jogData.velocityFactor = 8;
        txSerial(ARM_CMD_JOG, (uint8_t*)&jogData, sizeof(jogData), 0);

        serprintln("ARM_CMD_SETPOS");
        struct SETPOS_DATA setPosData;
        setPosData.joint = JOINT_PIVOT;
        setPosData.fAng = 0.0;
        txSerial(ARM_CMD_SETPOS, (uint8_t*)&setPosData, sizeof(setPosData));
        break;
    }
    case ARM_CMD_DOOR_EXT: {
        moveTo.x = 0;
        moveTo.y = 0;
        moveTo.z = 448;
        moveTo.angR = -90;
        moveTo.angG = 90;
        moveTo.velocityFactor = 1;
        txSerial(ARM_CMD_MOVETO, (uint8_t*)&moveTo, sizeof(moveTo), 1000);
        break;
    }
    case ARM_CMD_EXTEND: {
        moveTo.x = 0;
        moveTo.y = -150;
        moveTo.z = 400;
        moveTo.angR = 0;
        moveTo.angG = 90;
        moveTo.velocityFactor = 1;
        txSerial(ARM_CMD_MOVETO, (uint8_t*)&moveTo, sizeof(moveTo), 1000);
        break;
    }
    case ARM_CMD_TOOL_READY: {
        moveTo.x = 0;
        moveTo.y = -100;
        moveTo.z = 380;
        moveTo.angR = 0;
        moveTo.angG = 90;
        moveTo.velocityFactor = 1;
        txSerial(ARM_CMD_MOVETO, (uint8_t*)&moveTo, sizeof(moveTo), 1000);
        break;
    }
    case ARM_CMD_DOOR_PRESS: {
        serprintln("ARM_CMD_DOOR_PRESS Move slowly to right");
        struct JOG_DATA jogData;
        jogData.fDelta = 30;
        jogData.velocityFactor = 8;
        txSerial(ARM_CMD_JOG, (uint8_t*)&jogData, sizeof(jogData));
        break;
    }
    case GRIP_CMD_OPEN:
        SerialUSB.println("open");
        buf[0] = 0x3A;
        buf[1] = 255;
        buf[2] = ds1wire.crc8(buf[1]);
        ds1wire.send(addrGripper, buf, 3);
        break;
    case GRIP_CMD_CLOSE:
        SerialUSB.println("close");
        buf[0] = 0x3A;
        buf[1] = 1;
        buf[2] = ds1wire.crc8(buf[1]);
        ds1wire.send(addrGripper, buf, 3);
        break;
    default:
        if (cmd < ARM_CMD_DISABLE) {
            txSerial(cmd);
        }
    }
}

void grabCan()
{
    int16_t canX;
    int16_t canY;
    int16_t canZ;

    struct MOVETO_DATA moveTo;

    if (grabCanStatus == GRAB_CAN_RUNNING) {
        serprintln("Already running!");
        return;
    }

    grabCanStatus = GRAB_CAN_RUNNING;

    // get status
    getStatus();
    // if not init'd
    if (!driverStatus.state) {
        serprintln("ARM_CMD_INIT");
        txSerial(ARM_CMD_INIT);
    }
    waitWhileMoving();

    serprintln("start can_detect");
    buf[0] = VISION_PGM;
    buf[1] = VISION_PGM_CAN;
    sendCANMsg(CAN_CMD_VISION, 2, buf);
    yDelay(2000);

    txSerial(ARM_CMD_UNPARK);

    serprintln("open");
    buf[0] = 0x3A;
    buf[1] = 255;
    buf[2] = ds1wire.crc8(buf[1]);
    ds1wire.send(addrGripper, buf, 3);

    // Wait for 5 non-zero values then tell controller to move to intermediate location
    for (;;) {
        yield();
        if (millis() - lastCanUpdate < 1000 && medianCanX.isFull()) {
            vd("canX", medianCanX.getMedian());
            vd("canY", medianCanY.getMedian());
            vd("canZ", medianCanZ.getMedian());

            canX = medianCanX.getMedian() - WORLD_OFFSET_X;
            canY = medianCanY.getMedian() - WORLD_OFFSET_Y - 0;
            canZ = medianCanZ.getMedian() - WORLD_OFFSET_Z + 0;

            // Intermediate location
            moveTo.y = canY + 0;
            moveTo.z = canZ - 80;
            moveTo.x = round(((float)moveTo.z / (float)canZ) * canX);
            moveTo.angR = 0;
            moveTo.angG = 90 - 7.5; // - 15;
            moveTo.velocityFactor = 1;
            vd("moveTo.x", moveTo.x);
            vd("moveTo.y", moveTo.y);
            vd("moveTo.z", moveTo.z);
            txSerial(ARM_CMD_MOVETO, (uint8_t*)&moveTo, sizeof(moveTo), -1);
            if (driverStatus.err) {
                vd("driver err 1", driverStatus.err);
                grabCanStatus = GRAB_CAN_DRV_ERR;
            }
            break; // Move on to next state
        }
    }

    serprintln("stop can_detect");
    buf[0] = VISION_PGM;
    buf[1] = VISION_PGM_IDLE;
    sendCANMsg(CAN_CMD_VISION, 2, buf);

    if(grabCanStatus == GRAB_CAN_DRV_ERR) return;

    // Wait for motion to stop
    for (;;) {
        yield();
        getStatus();
        if (driverStatus.err) {
            vd("driver err 2", driverStatus.err);
            grabCanStatus = GRAB_CAN_DRV_ERR;
            return;
        } else if (driverStatus.moving)
            // serprint('.');
            ;
        else {
            serprintln("Done");
            break; // Move on to next state
        }
    }

    serprintln("start grip_detect");
    buf[0] = VISION_PGM;
    buf[1] = VISION_PGM_GRIP;
    sendCANMsg(CAN_CMD_VISION, 2, buf);
    yDelay(2000);

    int8_t adjX = 0;
    // int8_t adjY = 0;
    // int8_t adjZ = 0;

    // Wait for 5 non-zero values then tell controller to move to can
    for (;;) {
        yield();
        serprint(millis() - lastGripUpdate);
        if (millis() - lastGripUpdate < 1000 && medianGripX.isFull()) {
            vd("gripX", medianGripX.getMedian());
            // vd("gripY", medianGripY.getMedian());
            // vd("gripZ", medianGripZ.getMedian());

            int16_t gripX = medianGripX.getMedian() - WORLD_OFFSET_X;
            // int16_t gripY = medianGripY.getMedian() - WORLD_OFFSET_Y;
            // int16_t gripZ = medianGripZ.getMedian() - WORLD_OFFSET_Z;

            adjX = (gripX - canX);
            // adjY = canY - gripY;
            // adjZ = canZ - gripZ;
            vd("adjX", adjX);
            // vd("adjY", adjY);
            // vd("adjZ", adjZ);

            moveTo.x -= adjX;
            moveTo.y -= 60;
            // moveTo.z = canZ + adjZ;
            moveTo.z = canZ - 20;
            // moveTo.x = round(((float)moveTo.z / (float)canZ) * canX);
            moveTo.angG = 90;
            moveTo.velocityFactor = 2;
            vd("moveTo.x", moveTo.x);
            vd("moveTo.y", moveTo.y);
            vd("moveTo.z", moveTo.z);
            txSerial(ARM_CMD_MOVETO, (uint8_t*)&moveTo, sizeof(moveTo), -1);
            if (driverStatus.err) {
                vd("driver err 3", driverStatus.err);
                grabCanStatus = GRAB_CAN_DRV_ERR;
            }
            break; // Move on to next state
        }
    }

    serprintln("stop grip_detect");
    buf[0] = VISION_PGM;
    buf[1] = VISION_PGM_IDLE;
    sendCANMsg(CAN_CMD_VISION, 2, buf);

    if(grabCanStatus == GRAB_CAN_DRV_ERR) return;

    // Wait for motion to stop
    for (;;) {
        yield();
        getStatus();
        if (driverStatus.err) {
            vd("driver err 4", driverStatus.err);
            grabCanStatus = GRAB_CAN_DRV_ERR;
            return;
        } else if (driverStatus.moving)
            // serprint('.');
            ;
        else {
            serprintln("Done");
            break; // Move on to next state
        }
    }

    serprintln("close");
    buf[0] = 0x3A;
    buf[1] = 1;
    buf[2] = ds1wire.crc8(buf[1]);
    ds1wire.send(addrGripper, buf, 3);

    yDelay(1000);

    // Lift can
    moveTo.y += 50;
    vd("moveTo.x", moveTo.x);
    vd("moveTo.y", moveTo.y);
    vd("moveTo.z", moveTo.z);
    txSerial(ARM_CMD_MOVETO, (uint8_t*)&moveTo, sizeof(moveTo), -1);
    if (driverStatus.err) {
        vd("driver err 5", driverStatus.err);
        grabCanStatus = GRAB_CAN_DRV_ERR;
        return;
    }

    // Move to center
    moveTo.x = 0 - WORLD_OFFSET_X;
    moveTo.y = 300 - WORLD_OFFSET_Y;
    moveTo.z = 218 - WORLD_OFFSET_Z;
    moveTo.angR = 90;
    vd("moveTo.x", moveTo.x);
    vd("moveTo.y", moveTo.y);
    vd("moveTo.z", moveTo.z);
    vd("moveTo.angR", moveTo.angR);
    txSerial(ARM_CMD_MOVETO, (uint8_t*)&moveTo, sizeof(moveTo), -1);
    if (driverStatus.err) {
        vd("driver err 6", driverStatus.err);
        grabCanStatus = GRAB_CAN_DRV_ERR;
        return;
    }

    // Wait for motion to stop
    for (;;) {
        yield();
        getStatus();
        if (driverStatus.err) {
            vd("driver err 7", driverStatus.err);
            grabCanStatus = GRAB_CAN_DRV_ERR;
            return;
        } else if (driverStatus.moving)
            // serprint('.');
            ;
        else {
            serprintln("Done");
            break; // Move on to next state
        }
    }

    grabCanStatus = GRAB_CAN_SUCCESS;
}