#ifndef CHECKSERIAL_H
#define CHECKSERIAL_H

#include <Arduino.h>
#include "main.h"

#define RX_IDLE 0x00
#define RX_START_RECD 0x01

void checkSerial();
void txSerial(uint8_t cmd, int16_t msDelay = -1);
void txSerial(uint8_t cmd, uint8_t val, int16_t msDelay = -1);
void txSerial(uint8_t cmd, uint8_t buf[], uint8_t bytes, int16_t msDelay = -1);
void getStatus();
void waitWhileMoving();

#endif