#include "AccelStepperDriver.h"

AccelStepperDriver::AccelStepperDriver(uint8_t stepPin, uint8_t dirPin, uint8_t enPin) : AccelStepper(AccelStepper::DRIVER)
{
    _stepPin = stepPin;
    _dirPin = dirPin;
    // _enMask = 1 << enPin;
    _step1 = &(PORT->Group[g_APinDescription[stepPin].ulPort].OUTSET.reg);
    _step0 = &(PORT->Group[g_APinDescription[stepPin].ulPort].OUTCLR.reg);
    _stepMask = digitalPinToBitMask(stepPin);
    _dir1 = &(PORT->Group[g_APinDescription[dirPin].ulPort].OUTSET.reg);
    _dir0 = &(PORT->Group[g_APinDescription[dirPin].ulPort].OUTCLR.reg);
    _dirMask = digitalPinToBitMask(dirPin);
    _en1 = &(PORT->Group[g_APinDescription[enPin].ulPort].OUTSET.reg);
    _en0 = &(PORT->Group[g_APinDescription[enPin].ulPort].OUTCLR.reg);
    _enMask = digitalPinToBitMask(enPin);

    pinMode(stepPin, OUTPUT);
    pinMode(dirPin, OUTPUT);
    pinMode(enPin, OUTPUT);
}

// boolean AccelStepperDriver::runSpeed() {
//     boolean ret = AccelStepper::runSpeed();
//     if(ret) *_step0 = _stepMask;
//     return ret;
// }

/*
inline void AccelStepperDriver::step(long step)
{
    if(_direction == DIRECTION_CW) {
        *_dir1 = _dirMask;
    } else {
        *_dir0 = _dirMask;
    }
    *_step1 = _stepMask;
    delayMicroseconds(20);
    *_step0 = _stepMask;
}
*/

void AccelStepperDriver::setOutputPins(uint8_t mask)
{
    digitalWrite(_stepPin, (mask & 0b01));
    digitalWrite(_dirPin, (mask & 0b10));
}

// void AccelStepperDriver::step(long step)
// {
//     (void)(step); // Unused

//     // _pin[0] is step, _pin[1] is direction
//     setOutputPins(_direction ? 0b10 : 0b00); // Set direction first else get rogue pulses
//     setOutputPins(_direction ? 0b11 : 0b01); // step HIGH
//     // Caution 200ns setup time
//     // Delay the minimum allowed pulse width
//     delayMicroseconds(1);
//     setOutputPins(_direction ? 0b10 : 0b00); // step LOW
// }

// End
