#ifndef ACCELSTEPPERDRIVER_h
#define ACCELSTEPPERDRIVER_h

#include <Arduino.h>
#include <AccelStepper.h>

class AccelStepperDriver : public AccelStepper {

public:
    AccelStepperDriver(uint8_t stepPin, uint8_t dirPin, uint8_t enPin);
    // boolean runSpeed();
    void setOutputPins(uint8_t mask);
    // void step(long step);

private:
    uint8_t _stepPin;
    uint8_t _dirPin;
    // uint32_t _enMask;
	volatile uint32_t* _step1;
	volatile uint32_t* _step0;
    uint32_t _stepMask;
	volatile uint32_t* _dir1;
	volatile uint32_t* _dir0;
    uint32_t _dirMask;
	volatile uint32_t* _en1;
	volatile uint32_t* _en0;
    uint32_t _enMask;
};

#endif

// End
